<%@ page contentType="text/html;charset=UTF-8" language="java" isELIgnored="false" %>
<!DOCTYPE html>
<html>
  
  <head>
    <meta charset="UTF-8">
    <title>员工列表</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/font.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/xadmin.css">
    <script type="text/javascript" src="https://cdn.bootcss.com/jquery/3.2.1/jquery.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/static/lib/layui/layui.js" charset="utf-8"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/static/js/xadmin.js"></script>
    <!-- 让IE8/9支持媒体查询，从而兼容栅格 -->
    <!--[if lt IE 9]>
      <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
      <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  
  <body>
    <div class="x-nav">
      <span class="layui-breadcrumb">
        <a href="">首页</a>
        <a href="">员工</a>
        <a>
          <cite>列表</cite></a>
      </span>
      <a class="layui-btn layui-btn-small" style="line-height:1.6em;margin-top:3px;float:right" href="javascript:location.replace(location.href);" title="刷新">
        <i class="layui-icon" style="line-height:30px">ဂ</i></a>
    </div>
    <div class="x-body">
      <div class="layui-row">
        <form class="layui-form layui-col-md12 x-so" onsubmit="return false;">
          <input class="layui-input" placeholder="开始日" name="startTime" id="start">
          <input class="layui-input" placeholder="截止日" name="endTime" id="end">
          <input type="text" name="keywords"  placeholder="请输入关键字" autocomplete="off" class="layui-input">
          <button class="layui-btn" id="search" ><i class="layui-icon">&#xe615;</i></button>
        </form>
      </div>
      <xblock class="do-table">
        <button class="layui-btn layui-btn-danger" data-type="deleteAll"><i class="layui-icon"></i>批量删除</button>
        <button class="layui-btn" onclick="x_admin_show('添加员工','${pageContext.request.contextPath}/empl/add/page')"><i class="layui-icon"></i>添加</button>
      </xblock>
      <table id="data" lay-filter="empl"></table>


    </div>
    <script>
      var table;
      layui.use(['laydate','table'], function(){
        var laydate = layui.laydate;
        table = layui.table;
        //执行一个laydate实例
        laydate.render({
          elem: '#start' //指定元素
        });
        //执行一个laydate实例
        laydate.render({
          elem: '#end' //指定元素
        });

        table.render({
          elem: '#data'
          ,height: 600
          ,url: '${pageContext.request.contextPath}/empl/list/data' //数据接口
          ,method: "post"
          ,page: true //开启分页
          ,cols: [[ //表头
            {type:'checkbox', fixed: 'left'}
            ,{field: 'id', title: 'ID', width:'5%', sort: true}
            ,{field: 'emplName', title: '登录名', width:'10%'}
            ,{field: 'emplRealName', title: '真名', width:'10%'}
            ,{field: 'emplGender', title: '性别', width:'5%', templet:function (d) {
                  return d.emplGender === 1 ? '男' : '女';
              }}
            ,{field: 'emplPhone', title: '手机号', width:'15%'}
            ,{field: 'deptId', title: '部门', width: '10%', templet:function (d) {
                return d.dept.deptName;
              }}
            ,{field: 'emplStatus', title: '状态', width: '10%', templet:function (d) {
                  var status = d.emplStatus === 1 ? '' : 'layui-btn-danger';
                  var statusText = d.emplStatus === 1 ? '启用' : '禁用';
                  return '<button type="button" class="layui-btn layui-btn-sm '+status+'">'+statusText+'</button>';
              }}
            ,{field: 'createTime', title: '创建时间', width: '15%', sort: true}
            ,{field: 'lastLoginTime', title: '最后登录时间', width: '15%', sort: true, templet:function (d) {
                  return d.lastLoginTime==null ? '未登录' : d.lastLoginTime;
              }}
            ,{field: 'updateTime', title: '更新时间', width: '20%'}
            ,{fixed: 'right',title: '操作', width:'20%', align:'center', toolbar: '#do'}
          ]]
        });
        //工具条事件
        table.on('tool(empl)', function(obj){ //注：tool 是工具条事件名，test 是 table 原始容器的属性 lay-filter="对应的值"
          var data = obj.data; //获得当前行数据
          var layEvent = obj.event; //获得 lay-event 对应的值（也可以是表头的 event 参数对应的值）
          var tr = obj.tr; //获得当前行 tr 的 DOM 对象（如果有的话）

          if(layEvent === 'status'){ //改变状态
            // 先改变状态
            data.emplStatus = data.emplStatus === 0 ? 1 : 0;
            var emplData = {id:data.id, emplStatus: data.emplStatus}
            // 发送ajax改变状态
            $.ajax({
              type:"post",
              url:"${pageContext.request.contextPath}/empl/change/status",
              data: emplData,
              dataType: "json",
              success:function (res) {
                if (res.code === 200){
                    // 重新加载表格
                    table.reload("data");
                }
              }
            });

          } else if(layEvent === 'del'){ //删除
            layer.confirm('真的删除行么', function(index){
              obj.del(); //删除对应行（tr）的DOM结构，并更新缓存
              layer.close(index);
              //向服务端发送删除指令
            });
          } else if(layEvent === 'edit'){ //编辑
            //do something
            x_admin_show('编辑员工','${pageContext.request.contextPath}/empl/edit/page?id='+data.id)

          } else if(layEvent === 'LAYTABLE_TIPS'){
            layer.alert('Hi，头部工具栏扩展的右侧图标。');
          }
        });
        var $ = layui.$, active = {
          deleteAll: function(){
            var checkStatus = table.checkStatus('data')
                    ,data = checkStatus.data;
            if (data.length === 0){
              layer.msg('请至少选中一行数据，进行删除！');
              return;
            }
            layer.confirm('确认要删除吗？',function(index){
              //获取选中数据中的id并组装成一个数组
              var ids = [];
              $(data).each(function () {
                ids.push(this.id);
              })
              // 发送请求到后端
              $.ajax({
                type:"post",
                url:"${pageContext.request.contextPath}/empl/delete",
                data: {ids: ids},
                dataType: "json",
                success:function (res) {
                  layer.msg(res.msg, {icon: 6},function () {
                    // 刷新父级的表格
                    table.reload('data');
                  });
                }
              });
            });
          }
        };

        $('.do-table .layui-btn-danger').on('click', function(){
          var type = $(this).data('type');
          active[type] ? active[type].call(this) : '';
        });

        // 搜索按钮
        $("#search").click(function () {
          //获取对应的数据
          // 获取关键字
          var keywords = $("input[name='keywords']").val();
          // 获取开始时间
          var startTime = $("#start").val();
          // 获取结束时间
          var endTime = $("#end").val();
          // 组装总共的提交参数
          var param = {keywords: keywords, startTime: startTime ? startTime+" 00:00:00" : '', endTime: endTime ? endTime+" 23:59:59": ''}

          // 表格数据的重载
          table.reload("data",{where: param})
        });
      });

    </script>


    <script type="text/html" id="do">
      <a class="layui-btn layui-btn-xs" lay-event="status">状态改变</a>
      <a class="layui-btn layui-btn-xs" lay-event="edit">编辑</a>
      <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del">删除</a>
    </script>

  </body>

</html>