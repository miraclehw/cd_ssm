<%@ page contentType="text/html;charset=UTF-8" language="java" isELIgnored="false" %>
<!DOCTYPE html>
<html>
  
  <head>
    <meta charset="UTF-8">
    <title>员工编辑</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/font.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/xadmin.css">
    <script type="text/javascript" src="https://cdn.bootcss.com/jquery/3.2.1/jquery.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/static/lib/layui/layui.js" charset="utf-8"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/static/js/xadmin.js"></script>
    <!-- 让IE8/9支持媒体查询，从而兼容栅格 -->
    <!--[if lt IE 9]>
      <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
      <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  
  <body>
    <div class="x-body">
        <form class="layui-form">
          <div class="layui-form-item">
              <label for="emplName" class="layui-form-label">
                  <span class="x-red">*</span>登录名
              </label>
              <div class="layui-input-inline">
                  <input type="text" id="emplName" name="emplName" required="" lay-verify="required"
                  autocomplete="off" class="layui-input">
              </div>
              <div class="layui-form-mid layui-word-aux">
                  <span class="x-red">*</span>将会成为您唯一的登入名
              </div>
          </div>
            <div class="layui-form-item">
                <label for="emplRealName" class="layui-form-label">
                    <span class="x-red">*</span>真实名字
                </label>
                <div class="layui-input-inline">
                    <input type="text" id="emplRealName" name="emplRealName" required="" lay-verify="required"
                           autocomplete="off" class="layui-input">
                </div>
                <div class="layui-form-mid layui-word-aux">
                </div>
            </div>
          <div class="layui-form-item">
              <label for="phone" class="layui-form-label">
                  <span class="x-red">*</span>手机
              </label>
              <div class="layui-input-inline">
                  <input type="text" id="phone" name="emplPhone" required="" lay-verify="phone"
                  autocomplete="off" class="layui-input">
              </div>
              <div class="layui-form-mid layui-word-aux">
              </div>
          </div>
          <div class="layui-form-item">
              <label  class="layui-form-label">
                  <span class="x-red">*</span>性别
              </label>
              <div class="layui-input-inline">
                  <input type="radio" name="emplGender" value="1" title="男" checked>
                  <input type="radio" name="emplGender" value="2" title="女" >
              </div>
              <div class="layui-form-mid layui-word-aux">
              </div>
          </div>
            <div class="layui-form-item">
                <label  class="layui-form-label">
                    <span class="x-red">*</span>部门
                </label>
                <div class="layui-input-inline">
                    <select name="deptId" lay-verify="required">
                        <option value=""></option>
                        <option value="0">北京</option>
                        <option value="1">上海</option>
                        <option value="2">广州</option>
                        <option value="3">深圳</option>
                        <option value="4">杭州</option>
                    </select>
                </div>
                <div class="layui-form-mid layui-word-aux">
                </div>
            </div>
          <div class="layui-form-item">
              <label class="layui-form-label"><span class="x-red">*</span>角色</label>
              <div class="layui-input-block" id="roles-box">
                <input type="checkbox" name="roles" lay-skin="primary" title="超级管理员" checked="">
                <input type="checkbox" name="roles" lay-skin="primary" title="编辑人员">
                <input type="checkbox" name="roles" lay-skin="primary" title="宣传人员" checked="">
              </div>
          </div>
          <div class="layui-form-item">
              <label for="L_pass" class="layui-form-label">
                  <span class="x-red">*</span>密码
              </label>
              <div class="layui-input-inline">
                  <input type="password" id="L_pass" name="emplPwd" required="" lay-verify="pass"
                  autocomplete="off" class="layui-input">
              </div>
              <div class="layui-form-mid layui-word-aux">
                  6到16个字符
              </div>
          </div>
          <div class="layui-form-item">
              <label for="L_repass" class="layui-form-label">
                  <span class="x-red">*</span>确认密码
              </label>
              <div class="layui-input-inline">
                  <input type="password" id="L_repass" name="repass" required="" lay-verify="repass"
                  autocomplete="off" class="layui-input">
              </div>
          </div>
          <div class="layui-form-item">
              <label for="L_repass" class="layui-form-label">
              </label>
              <button  class="layui-btn" lay-filter="add" lay-submit="">
                  添加
              </button>
          </div>
      </form>
    </div>
    <script>
        var from;
        layui.use(['form','layer'], function(){
            $ = layui.jquery;
           form = layui.form
          var layer = layui.layer;
        
          //自定义验证规则
          form.verify({
            nikename: function(value){
              if(value.length < 5){
                return '昵称至少得5个字符啊';
              }
            }
            ,pass: [/(.+){6,12}$/, '密码必须6到12位']
            ,repass: function(value){
                if($('#L_pass').val()!=$('#L_repass').val()){
                    return '两次密码不一致';
                }
            }
          });

          //监听提交
          form.on('submit(add)', function(data){
            var postData = data.field;
            //发异步，把数据提交给java
            // 单独获取 复选框的数据，转换成数组
            var roles = [];
            $("div[class='layui-unselect layui-form-checkbox layui-form-checked']").each(function () {
                // 找到input复选框
               var input = $(this).prev();
               var roleId = input.val();
               roles.push(roleId);
            })
            postData.roleIds = roles;
            console.log(postData);
            // 提交后台
              $.ajax({
                  type:"post",
                  url:"${pageContext.request.contextPath}/empl/add/do",
                  data: postData,
                  dataType: "json",
                  success:function (res) {
                      console.log(res);
                      layer.alert(res.msg, {icon: 6},function () {
                            // 刷新父级的表格
                            parent.table.reload('data');
                            // 获得frame索引
                            var index = parent.layer.getFrameIndex(window.name);
                            //关闭当前frame
                            parent.layer.close(index);

                      });
                  }
              });

            return false;
          });


            // 获取 部门列表
            $.ajax({
                type:"post",
                url:"${pageContext.request.contextPath}/dept/list/data",
                dataType: "json",
                success:function (res) {
                    if (res.code === 200){
                        $("select[name=deptId]").empty();
                        $(res.data).each(function () {
                            // 动态创建
                            var option = $('<option value="'+this.deptId+'">'+this.deptName+'</option>')
                            // 追加
                            $("select[name=deptId]").append(option);
                        })
                        // 重新渲染表单
                        form.render('select');
                    }
                }
            });

            // 获取 角色列表
            $.ajax({
                type:"post",
                url:"${pageContext.request.contextPath}/role/list/data",
                dataType: "json",
                success:function (res) {
                    console.log(res);
                    if (res.code === 200){
                        $("#roles-box").empty();
                        $(res.data).each(function () {
                            var input = $('<input type="checkbox" name="roleIds" lay-skin="primary" value="'+this.roleId+'" title="'+this.roleName+'">')
                            $("#roles-box").append(input);
                        })
                        // 重新渲染表单
                        form.render('checkbox');
                    }
                }
            });
          
          
        });

    </script>

  </body>

</html>