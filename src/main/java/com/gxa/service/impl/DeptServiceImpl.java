package com.gxa.service.impl;

import com.gxa.common.LayR;
import com.gxa.mapper.DeptMapper;
import com.gxa.pojo.Dept;
import com.gxa.service.DeptService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DeptServiceImpl implements DeptService {

    @Autowired
    private DeptMapper mapper;


    /**
     * 部门列表
     * @return
     */
    @Override
    public LayR list() {
        List<Dept> depts = mapper.findAll();
        return new LayR(200, "success", depts);
    }
}
