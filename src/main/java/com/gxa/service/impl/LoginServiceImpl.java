package com.gxa.service.impl;

import com.gxa.common.R;
import com.gxa.mapper.EmplMapper;
import com.gxa.pojo.Empl;
import com.gxa.pojo.Permission;
import com.gxa.pojo.Role;
import com.gxa.service.LoginService;
import com.gxa.util.MD5Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpSession;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Service
public class LoginServiceImpl implements LoginService {

    @Autowired
    private EmplMapper mapper;
    /**
     * 登录的方法
     * @param empl
     * @return
     */
    @Override
    public R login(Empl empl, HttpSession session) {
        // 数据校验

        // 从数据库中查询数据
        Empl dbEmpl = mapper.findByName(empl.getEmplName());
        // 判断用户是否存在
        if (dbEmpl == null) {
            return new R(1001, "该用户不存在！");
        }
        // 判断状态
        if (dbEmpl.getEmplStatus() != null && dbEmpl.getEmplStatus().equals(0)) {
            return new R(1001, "账户被锁定！请联系超级管理员！");
        }
        // 判断密码
        if (!dbEmpl.getEmplPwd().equals(MD5Util.MD55(empl.getEmplPwd()))) {
            return new R(1001, "密码错误！");
        }

        // 登录成功将用户信息存放到 session中
        session.setAttribute("emplName", dbEmpl.getEmplRealName());
        // 保存部门
        session.setAttribute("deptName", dbEmpl.getDept().getDeptName());
        // 保存角色
        session.setAttribute("roles", getRoleStr(dbEmpl));
        session.setAttribute("isSuper", isSuper(dbEmpl));
        // 保存当前用户拥有的权限
        if (!isSuper(dbEmpl)){
            session.setAttribute("permissions", getPermissions(dbEmpl));
        }

        // 更新登录时间
        empl.setId(dbEmpl.getId());
        empl.setLastLoginTime(new Timestamp(System.currentTimeMillis()));
        mapper.update(empl);

        return new R(200, "登录成功！");
    }

    /**
     * 获取角色信息
     * @param empl
     * @return
     */
    private List<Role> getRoles(Empl empl){
        return empl.getRoles();
    }

    /**
     * 判断是否是超级管理员
     * @param empl
     * @return
     */
    private Boolean isSuper(Empl empl){
        List<Role> roles = getRoles(empl);
        for (Role role : roles) {
            if (role.getRoleId().equals(1)) {
                return true;
            }
        }
        return false;
    }

    /**
     * 获取角色字符串
     * @param empl
     * @return
     */
    private String getRoleStr(Empl empl){
        List<Role> roles = getRoles(empl);
        String roleStr = "";
        for (Role role : roles) {
            roleStr += role.getRoleName()+",";
        }
        return roleStr.substring(0, roleStr.length()-1);
    }


    /**
     * 获取当前用户所拥有的权限
     * @param empl
     * @return
     */
    private List<Permission> getPermissions(Empl empl){
        List<Role> roles = getRoles(empl);
        List<Permission> permissions = new ArrayList<>();
        for (Role role : roles) {
            if (role.getPermissions() == null) {
                continue;
            }
            permissions.addAll(role.getPermissions());
        }
        return permissions;
    }
}
