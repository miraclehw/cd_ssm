package com.gxa.service.impl;

import com.gxa.common.LayR;
import com.gxa.mapper.RoleMapper;
import com.gxa.pojo.Role;
import com.gxa.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RoleServiceImpl implements RoleService {


    @Autowired
    private RoleMapper mapper;

    @Override
    public LayR list() {
        List<Role> roles = mapper.findAll();
        return new LayR(200,"success", roles);
    }
}
