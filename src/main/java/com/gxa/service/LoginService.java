package com.gxa.service;

import com.gxa.common.R;
import com.gxa.pojo.Empl;

import javax.servlet.http.HttpSession;

public interface LoginService {


    /**
     * 用户登录的方法
     * @param empl
     * @return
     */
    R login(Empl empl, HttpSession session);

}
