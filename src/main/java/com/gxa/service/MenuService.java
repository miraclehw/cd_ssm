package com.gxa.service;

import com.gxa.common.R;

import javax.servlet.http.HttpSession;

public interface MenuService {

    /**
     * 菜单的列表
     * @param session
     * @return
     */
    R menuList(HttpSession session);
}
