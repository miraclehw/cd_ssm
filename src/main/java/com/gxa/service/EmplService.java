package com.gxa.service;

import com.gxa.common.LayR;
import com.gxa.common.R;
import com.gxa.param.TableParam;
import com.gxa.pojo.Empl;

public interface EmplService {


    public LayR list(TableParam param);

    LayR changeStatus(Empl empl);

    /**
     * 添加员工
     * @param empl
     * @param roleIds
     * @return
     */
    R add(Empl empl, String[] roleIds);

    /**
     * 删除数据
     * @param ids
     * @return
     */
    R deleteAll(String[] ids);

    /**
     * 员工的详细信息
     * @param id
     * @return
     */
    R detail(Integer id);

}
