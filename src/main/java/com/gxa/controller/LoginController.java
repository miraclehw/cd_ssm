package com.gxa.controller;

import com.gxa.common.R;
import com.gxa.pojo.Empl;
import com.gxa.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;

/**
 *
 *开发的顺序：
 *   从后向前 ： 数据库->mapper->service->controller
 *
 *   用户登录： 输入用户名和密码
 *   给用户准确的提示： 正确的情况： 登录成功
 *                    错误情况：
 *                        该用户不存在！
 *                        密码错误！
 *                        账户被锁定，请联系超级管理员！
 *
 *  按照正常的理解：
 *      select * from empl where empl_name=? and empl_pwd=?
 *
 *      select * from empl where empl_name=?
 *
 *
 *
 *-----------------------------------------------------------------------
 *实现登录得拦截： 没有登录， 不允许访问后台得任何页面和方法
 *  利用拦截器来实现这个功能。
 *  1. 创建一个拦截器  implements HandlerInterceptor
 *      preHandle  ： 方法调用之前
 *          返回true: 表示放行
 *          返回false: 表示拦截
 *      postHandle ： 方法调用之后
 *      afterCompletion ： 整个方法调用完成之后（页面渲染完成）
 *  2. 注册拦截器
 *      <mvc:interceptors>
 *         <mvc:interceptor>
 *             <mvc:mapping path="/**"/>
 *             <bean class="com.gxa.interceptor.LoginInterceptor"/>
 *         </mvc:interceptor>
 *     </mvc:interceptors>
 *
 *--------------------------------------------------------------------------
 *
 * 用户退出： 真正的目的是需要清空保存在session中的数据
 *
 *
 *
 */
@Controller
@RequestMapping("login")
public class LoginController {

    @Autowired
    private LoginService loginService;

    /**
     * 登录页面
     * @return
     */
    @GetMapping("/page")
    public String page(){
        return "login";
    }


    /**
     * 登录的操作
     * @param empl
     * @return
     */
    @PostMapping("/do")
    @ResponseBody
    public R login(Empl empl, HttpSession session){
        return loginService.login(empl, session);
    }


    /**
     * 退出登录
     * @return
     */
    @PostMapping("/out")
    @ResponseBody
    public R logout(HttpSession session){
        // 清空数据
        session.removeAttribute("emplName");
        return new R(200, "退出成功！");
    }




}
