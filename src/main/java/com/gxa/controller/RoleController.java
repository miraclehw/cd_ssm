package com.gxa.controller;

import com.gxa.common.LayR;
import com.gxa.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/role")
public class RoleController {


    @Autowired
    private RoleService roleService;


    /**
     * 角色列表
     * @return
     */
    @PostMapping("/list/data")
    @ResponseBody
    public LayR list(){
        return roleService.list();
    }


}
