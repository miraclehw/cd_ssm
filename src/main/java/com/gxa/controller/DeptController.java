package com.gxa.controller;

import com.gxa.common.LayR;
import com.gxa.service.DeptService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/dept")
public class DeptController {



    @Autowired
    private DeptService deptService;


    /**
     * 部门列表
     * @return
     */
    @PostMapping("/list/data")
    @ResponseBody
    public LayR getList(){
        return deptService.list();

    }



}
