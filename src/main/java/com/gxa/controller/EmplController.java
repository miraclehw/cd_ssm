package com.gxa.controller;

import com.gxa.common.LayR;
import com.gxa.common.R;
import com.gxa.param.TableParam;
import com.gxa.pojo.Empl;
import com.gxa.service.EmplService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

/**
 * 这是员工的控制器
 * 这是rose
 * 这是jack
 * 这是tom
 */
@Controller
@RequestMapping("/empl")
public class EmplController {

    @Autowired
    private EmplService service;

    /**
     * 员工列表页面
     * @return
     */
    @GetMapping("/list/page")
    public String list(){
        return "empl/empl-list";
    }

    /**
     * 列表数据
     * @return
     */
    @PostMapping("/list/data")
    @ResponseBody
    public LayR  listData(TableParam param){
        return service.list(param);
    }


    /**
     * 改变状态
     * @return
     */
    @PostMapping("/change/status")
    @ResponseBody
    public LayR  listData(Empl empl){
        return service.changeStatus(empl);
    }


    /**
     * 员工的添加页面
     * @return
     */
    @GetMapping("/add/page")
    public String addPage(){
        return "empl/empl-add";
    }


    /**
     * 添加员工
     * @param empl
     * @param roleIds
     * @return
     */
    @PostMapping("/add/do")
    @ResponseBody
    public R add(Empl empl, @RequestParam("roleIds[]") String[] roleIds){
        return service.add(empl, roleIds);
    }


    /**
     * 员工的编辑页面
     * @return
     */
    @GetMapping("/edit/page")
    public String editPage(Integer id, Model model){
        model.addAttribute("id", id);
        return "empl/empl-edit";
    }

    /**
     * 详情
     * @param id
     * @return
     */
    @PostMapping("/detail")
    @ResponseBody
    public R detail(Integer id){
        return service.detail(id);
    }

    /**
     * 删除
     * @param ids
     * @return
     */
    @PostMapping("/delete")
    @ResponseBody
    public R delete(@RequestParam("ids[]") String[] ids){
        return service.deleteAll(ids);
    }
}
