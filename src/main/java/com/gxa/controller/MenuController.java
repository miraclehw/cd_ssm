package com.gxa.controller;


import com.gxa.common.R;
import com.gxa.service.MenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;

@RestController
public class MenuController {

    @Autowired
    private MenuService menuService;


    /**
     * 获取菜单
     * @param session
     * @return
     */
    @PostMapping("/menu/list")
    public R menuList(HttpSession session){
        return menuService.menuList(session);
    }


}
