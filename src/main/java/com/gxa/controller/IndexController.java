package com.gxa.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class IndexController {


    /**
     * 主页
     * @return
     */
    @GetMapping("/index")
    public String index(){
        return "index";
    }


    /**
     * 欢迎页面
     * @return
     */
    @GetMapping("/welcome")
    public String welcome(){
        return "welcome";
    }
}
