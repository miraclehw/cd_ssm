package com.gxa.pojo;


public class Permission {

  private Integer permissionId;
  private String permissionName;
  private String permissionUrl;
  private Integer isMenu;
  private Integer permissionLevel;
  private Integer parentId;


  public Permission() {
  }

  public Integer getPermissionId() {
    return permissionId;
  }

  public void setPermissionId(Integer permissionId) {
    this.permissionId = permissionId;
  }


  public String getPermissionName() {
    return permissionName;
  }

  public void setPermissionName(String permissionName) {
    this.permissionName = permissionName;
  }


  public String getPermissionUrl() {
    return permissionUrl;
  }

  public void setPermissionUrl(String permissionUrl) {
    this.permissionUrl = permissionUrl;
  }


  public Integer getIsMenu() {
    return isMenu;
  }

  public void setIsMenu(Integer isMenu) {
    this.isMenu = isMenu;
  }


  public Integer getPermissionLevel() {
    return permissionLevel;
  }

  public void setPermissionLevel(Integer permissionLevel) {
    this.permissionLevel = permissionLevel;
  }


  public Integer getParentId() {
    return parentId;
  }

  public void setParentId(Integer parentId) {
    this.parentId = parentId;
  }


  @Override
  public String toString() {
    return "Permission{" +
            "permissionId=" + permissionId +
            ", permissionName='" + permissionName + '\'' +
            ", permissionUrl='" + permissionUrl + '\'' +
            ", isMenu=" + isMenu +
            ", permissionLevel=" + permissionLevel +
            ", parentId=" + parentId +
            '}';
  }
}
