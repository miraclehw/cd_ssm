package com.gxa.pojo;


import com.fasterxml.jackson.annotation.JsonFormat;

import java.sql.Timestamp;
import java.util.List;

public class Empl {

  private Integer id;
  private String emplName;
  private String emplRealName;
  private String emplPwd;
  private Long emplPhone;
  private Integer emplGender;
  private Integer deptId;
  private Integer emplStatus;
  @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
  private Timestamp lastLoginTime;
  @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
  private Timestamp createTime;
  @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
  private Timestamp updateTime;
  private Integer isDeleted;

  /**
   * 当前员工所属部门
   */
  private Dept dept;

  /**
   * 当前员工拥有的角色
   */
  private List<Role> roles;

  public Empl() {
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }


  public String getEmplName() {
    return emplName;
  }

  public void setEmplName(String emplName) {
    this.emplName = emplName;
  }


  public String getEmplRealName() {
    return emplRealName;
  }

  public void setEmplRealName(String emplRealName) {
    this.emplRealName = emplRealName;
  }


  public String getEmplPwd() {
    return emplPwd;
  }

  public void setEmplPwd(String emplPwd) {
    this.emplPwd = emplPwd;
  }


  public Long getEmplPhone() {
    return emplPhone;
  }

  public void setEmplPhone(Long emplPhone) {
    this.emplPhone = emplPhone;
  }


  public Integer getEmplGender() {
    return emplGender;
  }

  public void setEmplGender(Integer emplGender) {
    this.emplGender = emplGender;
  }


  public Integer getDeptId() {
    return deptId;
  }

  public void setDeptId(Integer deptId) {
    this.deptId = deptId;
  }


  public Integer getEmplStatus() {
    return emplStatus;
  }

  public void setEmplStatus(Integer emplStatus) {
    this.emplStatus = emplStatus;
  }


  public Timestamp getLastLoginTime() {
    return lastLoginTime;
  }

  public void setLastLoginTime(Timestamp lastLoginTime) {
    this.lastLoginTime = lastLoginTime;
  }


  public Timestamp getCreateTime() {
    return createTime;
  }

  public void setCreateTime(Timestamp createTime) {
    this.createTime = createTime;
  }


  public Timestamp getUpdateTime() {
    return updateTime;
  }

  public void setUpdateTime(Timestamp updateTime) {
    this.updateTime = updateTime;
  }


  public Integer getIsDeleted() {
    return isDeleted;
  }

  public void setIsDeleted(Integer isDeleted) {
    this.isDeleted = isDeleted;
  }

  public Dept getDept() {
    return dept;
  }

  public void setDept(Dept dept) {
    this.dept = dept;
  }


  public List<Role> getRoles() {
    return roles;
  }

  public void setRoles(List<Role> roles) {
    this.roles = roles;
  }

  @Override
  public String toString() {
    return "Empl{" +
            "id=" + id +
            ", emplName='" + emplName + '\'' +
            ", emplRealName='" + emplRealName + '\'' +
            ", emplPwd='" + emplPwd + '\'' +
            ", emplPhone=" + emplPhone +
            ", emplGender=" + emplGender +
            ", deptId=" + deptId +
            ", emplStatus=" + emplStatus +
            ", lastLoginTime=" + lastLoginTime +
            ", createTime=" + createTime +
            ", updateTime=" + updateTime +
            ", isDeleted=" + isDeleted +
            ", dept=" + dept +
            ", roles=" + roles +
            '}';
  }
}
