package com.gxa.pojo;


import java.sql.Timestamp;
import java.util.List;

public class Role {

  private Integer roleId;
  private String roleName;
  private String roleDesc;
  private Integer deptId;
  private Integer opertorEmplId;
  private Timestamp createTime;
  private Timestamp updateTime;

  private List<Permission> permissions;

  public Role() {
  }

  public Integer getRoleId() {
    return roleId;
  }

  public void setRoleId(Integer roleId) {
    this.roleId = roleId;
  }


  public String getRoleName() {
    return roleName;
  }

  public void setRoleName(String roleName) {
    this.roleName = roleName;
  }


  public String getRoleDesc() {
    return roleDesc;
  }

  public void setRoleDesc(String roleDesc) {
    this.roleDesc = roleDesc;
  }


  public Integer getDeptId() {
    return deptId;
  }

  public void setDeptId(Integer deptId) {
    this.deptId = deptId;
  }


  public Integer getOpertorEmplId() {
    return opertorEmplId;
  }

  public void setOpertorEmplId(Integer opertorEmplId) {
    this.opertorEmplId = opertorEmplId;
  }


  public Timestamp getCreateTime() {
    return createTime;
  }

  public void setCreateTime(Timestamp createTime) {
    this.createTime = createTime;
  }


  public Timestamp getUpdateTime() {
    return updateTime;
  }

  public void setUpdateTime(Timestamp updateTime) {
    this.updateTime = updateTime;
  }

  public List<Permission> getPermissions() {
    return permissions;
  }

  public void setPermissions(List<Permission> permissions) {
    this.permissions = permissions;
  }

  @Override
  public String toString() {
    return "Role{" +
            "roleId=" + roleId +
            ", roleName='" + roleName + '\'' +
            ", roleDesc='" + roleDesc + '\'' +
            ", deptId=" + deptId +
            ", opertorEmplId=" + opertorEmplId +
            ", createTime=" + createTime +
            ", updateTime=" + updateTime +
            ", permissions=" + permissions +
            '}';
  }
}
