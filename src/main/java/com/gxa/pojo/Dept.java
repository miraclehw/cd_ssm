package com.gxa.pojo;


import java.sql.Timestamp;

public class Dept {

  private Integer deptId;
  private String deptName;
  private String deptDesc;
  private Integer opertorEmplId;
  private Timestamp createTime;
  private Timestamp updateTime;


  public Dept() {
  }

  public Integer getDeptId() {
    return deptId;
  }

  public void setDeptId(Integer deptId) {
    this.deptId = deptId;
  }


  public String getDeptName() {
    return deptName;
  }

  public void setDeptName(String deptName) {
    this.deptName = deptName;
  }


  public String getDeptDesc() {
    return deptDesc;
  }

  public void setDeptDesc(String deptDesc) {
    this.deptDesc = deptDesc;
  }


  public Integer getOpertorEmplId() {
    return opertorEmplId;
  }

  public void setOpertorEmplId(Integer opertorEmplId) {
    this.opertorEmplId = opertorEmplId;
  }


  public Timestamp getCreateTime() {
    return createTime;
  }

  public void setCreateTime(Timestamp createTime) {
    this.createTime = createTime;
  }


  public Timestamp getUpdateTime() {
    return updateTime;
  }

  public void setUpdateTime(Timestamp updateTime) {
    this.updateTime = updateTime;
  }


  @Override
  public String toString() {
    return "Dept{" +
            "deptId=" + deptId +
            ", deptName='" + deptName + '\'' +
            ", deptDesc='" + deptDesc + '\'' +
            ", opertorEmplId=" + opertorEmplId +
            ", createTime=" + createTime +
            ", updateTime=" + updateTime +
            '}';
  }
}
