package com.gxa.pojo;


public class EmplRole {

  private Integer id;
  private Integer emplId;
  private Integer roleId;

  public EmplRole() {
  }

  public EmplRole(Integer emplId, Integer roleId) {
    this.emplId = emplId;
    this.roleId = roleId;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }


  public Integer getEmplId() {
    return emplId;
  }

  public void setEmplId(Integer emplId) {
    this.emplId = emplId;
  }


  public Integer getRoleId() {
    return roleId;
  }

  public void setRoleId(Integer roleId) {
    this.roleId = roleId;
  }

}
