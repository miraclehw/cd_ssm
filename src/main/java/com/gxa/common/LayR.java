package com.gxa.common;

/**
 * Created with IntelliJ IDEA.
 * User: admin
 * Description: 用于layUI的数据表格的统一返回数据
 * Version: V1.0
 */
@SuppressWarnings("all")
public class LayR<T> {
    private int code; //状态码
    private String msg; //返回消息
    private Long count; // 总的条数
    private T data; //返回数据(用泛型表示)


    public LayR() {
    }

    public LayR(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public LayR(int code, String msg, T data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
    }

    public LayR(int code, String msg, Long count, T data) {
        this.code = code;
        this.msg = msg;
        this.count = count;
        this.data = data;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }


    public Long getCount() {
        return count;
    }

    public void setCount(Long count) {
        this.count = count;
    }
}
