package com.gxa.interceptor;

import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * 登录拦截器
 */
public class LoginInterceptor implements HandlerInterceptor {

    /**
     * 拦截前处理
     * @param request
     * @param response
     * @param o
     * @return
     * @throws Exception
     */
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object o) throws Exception {
        // 拦截器的逻辑
        // 通过什么来判断当前的请求是否登录了？
        // 通过session
        // 获取session对象
        HttpSession session = request.getSession();
        // 获取session中存放的数据
        String emplName = (String) session.getAttribute("emplName");
        // 判断存放的数据是否存在？ 存在就表示已经登录了， 不存在说明没有登录
        // 存在 =》 直接放行
        // 不存在 =》 重定向到登陆页面
        if (emplName == null) {
            // 重定向到登录页面
            response.sendRedirect("/login/page");
            return false;
        }
        // 放行
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, ModelAndView modelAndView) throws Exception {

    }

    @Override
    public void afterCompletion(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, Exception e) throws Exception {

    }
}
