package com.gxa.mapper;

import com.gxa.pojo.Dept;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface DeptMapper {


    /**
     * 通过主键查询数据
     * @param deptId
     * @return
     */
    Dept findByPk(@Param("deptId") Integer deptId);

    /**
     * 部门列表
     * @return
     */
    List<Dept> findAll();


}
