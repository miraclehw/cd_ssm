package com.gxa.mapper;

import com.gxa.param.TableParam;
import com.gxa.pojo.Empl;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface EmplMapper {

    /**
     * 查询所有
     * @return
     */
    List<Empl> findAll(@Param("param") TableParam param);

    /**
     * 通过主键查询数据
     * @param id
     * @return
     */
    Empl findByPk(@Param("id") Integer id);


    /**
     * 添加数据
     * @param empl
     */
    void save(@Param("empl") Empl empl);


    /**
     * 删除
     * @param ids
     */
    void deleteAll(@Param("ids") String[] ids);


    /**
     * 通过用户名查询
     * @param emplName
     * @return
     */
    Empl findByName(@Param("emplName") String emplName);

    /**
     * 更新用户
     * @param empl
     */
    void update(@Param("empl") Empl empl);

}
