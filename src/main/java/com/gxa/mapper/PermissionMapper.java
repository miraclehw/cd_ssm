package com.gxa.mapper;

import com.gxa.pojo.Permission;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface PermissionMapper {


    /**
     * 通过角色查找权限
     * @param roleId
     * @return
     */
    List<Permission> findByRoleId(@Param("roleId") Integer roleId);


    /**
     * 所有的权限
     * @return
     */
    List<Permission> findAll();


}
