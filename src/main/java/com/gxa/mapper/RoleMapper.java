package com.gxa.mapper;

import com.gxa.pojo.Role;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface RoleMapper {


    /**
     * 通过员工的id查询角色
     * @param emplId
     * @return
     */
    List<Role> findByEmplId(@Param("emplId") Integer emplId);

    /**
     * 查询所有
     * @return
     */
    List<Role> findAll();

}
