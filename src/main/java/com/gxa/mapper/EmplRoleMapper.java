package com.gxa.mapper;

import com.gxa.pojo.Empl;
import com.gxa.pojo.EmplRole;
import org.apache.ibatis.annotations.Param;

public interface EmplRoleMapper {

    /**
     * 添加关系表
     * @param emplRole
     */
    void save(@Param("emplRole") EmplRole emplRole);

}
