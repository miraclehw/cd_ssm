import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.gxa.mapper.EmplMapper;
import com.gxa.mapper.PermissionMapper;
import com.gxa.pojo.Empl;
import com.gxa.pojo.Menu;
import com.gxa.pojo.Permission;
import com.gxa.pojo.Role;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.lang.reflect.Member;
import java.util.ArrayList;
import java.util.List;

// 指定spring的测试环境
@RunWith(SpringJUnit4ClassRunner.class)
// 指定加在的配置文件
@ContextConfiguration(locations = {"classpath:spring_config.xml"})
public class MyTest {


    @Autowired
    private EmplMapper mapper;

    @Autowired
    private PermissionMapper permissionMapper;



    @Test
    public void test01(){
        Empl rose = mapper.findByName("rose");
        System.out.println(rose);
        for (Role role : rose.getRoles()) {
            System.out.println(role);
        }
    }


    @Test
    public void test02(){
        // 如何将权限转换成菜单？
        // 先获取一级菜单
        // 再通过一级菜单获取二级菜单

        List<Permission> permissions = permissionMapper.findAll();

        List<Menu> menus = new ArrayList<>();

        for (Permission permission : permissions) {
            // 判断是否是一级菜单
            if (permission.getParentId().equals(0) && permission.getPermissionLevel().equals(1) && permission.getIsMenu().equals(1)) {
                menus.add(new Menu(permission.getPermissionId(), permission.getPermissionName(), permission.getPermissionUrl(), permission.getParentId()));
            }
        }

        // 通过一级权限获取二级菜单
        for (Menu menu : menus) {
            List<Menu> subMenus = new ArrayList<>();
            for (Permission permission : permissions) {
                // 添加二级菜单？
                // menu的id == permission的父级id, 说明当前这个permission 就是一个子菜单
                if (permission.getParentId().equals(menu.getMenuId()) && permission.getPermissionLevel().equals(2) && permission.getIsMenu().equals(1)){
                    subMenus.add(new Menu(permission.getPermissionId(), permission.getPermissionName(), permission.getPermissionUrl(), permission.getParentId()));
                }
            }
            // 子菜单
            menu.setSubMenus(subMenus);
        }

        for (Menu menu : menus) {
            System.out.println(menu);
        }
    }




    @Test
    public void test03(){
        PageHelper.startPage(2, 3);
        List<Empl> emplList = mapper.findAll(null);
        // 封装成pageInfo
        PageInfo<Empl> pageInfo = new PageInfo<>(emplList);
        // 总的条数获取到
        System.out.println(pageInfo.getTotal());

        for (Empl empl : pageInfo.getList()) {
            System.out.println(empl);
        }
    }
}
